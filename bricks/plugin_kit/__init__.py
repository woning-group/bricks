"""
Here are the utility classes and function that must be used
when writing a plugin.

The following class types are defined:

- init.py - Initialization steps
"""
