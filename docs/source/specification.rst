``bricks.yml`` reference
========================

The ``bricks.yml`` has the following sections: metadata, plugins and commands

The ``metadata`` section
------------------------

It contains static information about the project.

The following values can be defined in here:

- ``name``: the name of the project
- ``version``: the current version of the project
- ``tags``: a list of tags for the project. Each tag is a unique string.
- ``author`` (Optional): The name of the author. If not given, will default to
  "Anonymous".
- ``description``: A short description of the project

Example:

.. code-block:: yaml

    metadata:
        name: example
        version: 0.13.2
        tags:
        - python
        - backend
        author: John Doe
        description: This is an example metadata section

The ``plugins`` section
-----------------------

It contains a list of used plugins. Each entry must have a ``name`` and a
``params`` key.

Example:

.. code-block:: yaml

    plugins:
    - name: python_library
      params:
          index: https://mypypiclone.example.com/
    - name: bumpversion
    - name: python_django
      params:
          settings: myproject.settings
          create_initial_fixtures: true

The ``commands`` section
------------------------

Contains a list of project commands. Each entry must have the following keys

- ``name``: the name of the command which can be invoked via ``bricks run``
- ``description``: a short description of what this command does
- ``driver`` (optional, defaults to ``local``) : the executor for the command.
  Supports ``local`` or ``docker``
- ``commands``: a list of commands to be executed. Each command will
  have access to parameters as environment variables.

Example:

.. code-block:: yaml

    commands:

    - name: unit-tests
      description: Runs the unit tests
      driver: local
      commands:
      - pytest tests/ -m unit_test

    - name: integration-tests
      description: Runs the integration tests
      driver: docker
      commands:
      - pytest tests/ -m integration_test

    - name: deploy
      description: Deploys the application to the specified $environment
      driver: local
      commands:
      - /bin/bash scripts/deploy.sh $environment