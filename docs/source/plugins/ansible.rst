``ansible``
===========

Run ``ansible`` playbooks in different environments.

Initialization
--------------

Installs ``ansible`` and creates (if not present) the ``ansible``
directory with ``invetories`` directory (where all the inventories will be)
and the ``playbooks`` inventories.

By convention, in the ``ansible/inventories`` directory will be different
directories, each containing a ``hosts`` file representing the inventory for
the specific environment.

For example, in the ``ansible/inventories/staging/hosts`` will be the hosts
for the staging environment and in the ``ansible/inventories/prod/hosts``
will be the hosts for the production environment.

The playbooks must be in the ``ansible/playbooks`` directory with the ``.yml``
extension.

Exposed commands
----------------

For each environment and playbook combination, a command will be created
with the following name:

``ansible-{environment}-{playbook}``.

For example, for the ``ansible/inventories/staging/hosts`` and
``ansible/playbooks/deploy.yml`` combination, we will have the

``ansible-staging-deploy`` command generated.
