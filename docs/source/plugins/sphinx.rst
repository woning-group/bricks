``sphinx``
==========

Document your project using ``sphinx``.

Initialization
--------------

Installs ``sphinx`` and initializes the project in ``docs/`` (if not already
initialized).

The sources will be added in ``docks/source/``


Exposed commands
----------------

``build-docs``
~~~~~~~~~~~~~~

Builds the HTML output for the documentation.

``open-docs``
~~~~~~~~~~~~~

Opens the latest built HTML documentation in the default browser.


