``python_library``
==================

Exposes commands useful when developing a python library.

Initialization
--------------

Installs ``pytest`` (for runnings tests), ``pycodestyle`` (for linting)
and ``twine`` (for uploading packages to pypi.io).

Creates the following files if they do not exist:

- ``setup.py`` - needed for packaging
- ``tests/test_default.py`` - a default passing test. All tests must be in
  ``tests/``.
- ``{project.metadata.name}/__init__.py`` - the basic package.


Exposed commands
----------------

``test``
~~~~~~~~

Runs the test suite from ``tests/`` using ``pytest``. Runs locally.


``lint``
~~~~~~~~

Checks compliance to pep8 usint ``pycodestyle``.

``build-dist``
~~~~~~~~~~~~~~

Creates the distributable packages. By default creates a ``sdist`` and
``bdist_wheel``. The artifacts are generated in ``dist/``.

``upload``
~~~~~~~~~~

Uploads the artifacts that were built for the current version to pypi.io
using ``twine``. You will be prompted for username and password.
