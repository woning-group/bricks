Plugins
=======

.. toctree::
   :maxdepth: 2
   :caption:  Available commands

   ansible
   bumpversion
   dockercompose
   python_library
   sphinx