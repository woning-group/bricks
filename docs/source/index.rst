.. bricks documentation master file, created by
   sphinx-quickstart on Sun Jun 10 19:20:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bricks's documentation!
==================================

Bricks is a tool for easy development workflow management.

Its purpose is to provide an easy to remember and use wrapper around the
most usual development tools that you use.

Installation
------------

To install it, run

.. code-block:: bash

   pip install woning-bricks


Minimal configuration
---------------------

In order to use bricks, you have to put in the project root a ``bricks.yml``
file which has the following structure (this is the hello world equivalent)

.. code-block:: yaml

   metadata:
      name: hello-world-bricks
      version: 1.0.0
      author: John Doe
      description: Basic hello world example
      tags:
      - hello world
      - example

   plugins: []

   commands:
   - name: say-hello
     commands:
     - echo "Hello world from bricks!"
     description: Says hello world
     driver: local

   - name: say-hello-again
     commands:
     - echo "Hello again!"
     description: Says hello again
     driver: local

   # this ones takes a parameter. We'll see in a moment what that is about
   - name: say-hello-to-person
     commands:
     - echo "Hello $name"
     description: Says hello to someone
     driver: local

Using bricks
------------

Once you have ``bricks.yml``, you can run the following commands

.. code-block:: bash

   $ bricks help
   # displays info about the project and all the available commands

   $ bricks init
   # initializes the plugins (if you use any)

   $ bricks run say-hello
   # runs a single command

   $ bricks run say-hello say-hello-again
   # we can run them in succession

   $ bricks run say-hello-to-person -p name=John
   # we can parametrize them

   $ bricks autocomplete > autocomplete.sh
   # you can run the autocompletion init on your .bashrc file
   $ source autocomplete.sh
   # now we have autocompletion for bricks. Just double tap TAB as you write
   # bricks commands.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   specification
   plugins/index




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
