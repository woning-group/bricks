from setuptools import setup, find_packages

setup(
    name="example_lib",
    description="An example Python library",
    version="0.0.1",
    packages=find_packages(),
    install_requires=[
        # add the install dependencies here
    ]
)
