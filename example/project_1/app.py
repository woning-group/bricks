import os
from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return os.environ.get('message') or "hello there from Flask"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
