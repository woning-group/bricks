from bricks.plugin_kit.base import BasePlugin, native_command


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()

    @native_command()
    def say_hello_to(self, who='World'):
        """Says hello to somebody"""
        print("Hello {}".format(who))
