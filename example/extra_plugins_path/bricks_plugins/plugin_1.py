from bricks.plugin_kit.base import BasePlugin, native_command


class Plugin(BasePlugin):
    @native_command()
    def say_hello(self):
        """Says hello"""
        print("Hello there")
